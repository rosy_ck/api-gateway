package com.apigateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;


//@PropertySource("classpath:route-app.yaml")
/*
* IMPORTANTE: non bisogna inserire il riferimento alla spring-web
* perchè va in conflitto con la spring-cloud-gateway
*/
@SpringBootApplication
//@EnableDiscoveryClient
//@EnableFeignClients

public class ApiGatewayApplication {
    private final static Logger log = LoggerFactory.getLogger(ApiGatewayApplication.class);
    // curl http://host.docker.internal:8080/get --> json della richiesta di route
    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        log.info("[Api Gateway Routing]");
        System.out.println("sono nel gateway");
        //**** Esempio
        return builder.routes()
                //****SHOPS
                .route(p -> p
                        .path("/api/v1/shops").and().method("GET")
                        .uri("http://host.docker.internal:8085/api/v1/shops")
                )
                .route(p -> p
                        .path("/api/v1/shop").and().method("POST")
                        .filters(f -> f.addRequestHeader("content-type", "application/json"))
                        .uri("http://host.docker.internal:8085/api/v1/shop")
                )
                .route(p -> p
                        .path("/api/v1/shop/findByIds").and().method("POST")
                        .uri("http://host.docker.internal:8085/api/v1/shop/findByIds")
                )
                //agosto 22 2021
                .route(p -> p
                        .path("/api/v1/shop/**").and().method("GET")
                        .filters(f->f.rewritePath("api/v1/shop/(?<shopId>.*)","api/v1/shop/${shopId}"))
                        .uri("http://host.docker.internal:8085")
                )
                //****USER FARMER
                .route(p->p.
                        path("/api/v1/farmer").and().method("GET")
                        .uri("http://host.docker.internal:8085/api/v1/farmer")
                )
                //****USER
                .route(p -> p
                        .path("/api/v1/user/find").and().method("POST")
                        .uri("http://host.docker.internal:8085/api/v1/user/find")
                )
                .route(p -> p
                        .path("/api/v1/user/update").and().method("POST")
                        .uri("http://host.docker.internal:8085/api/v1/user/update")
                )
                .route(p -> p
                        .path("/api/v1/user").and().method("POST")
                        .uri("http://host.docker.internal:8085/api/v1/user")
                )
                //***Products
                .route(p -> p
                        .path("/api/v1/products").and().method("GET")
                        .uri("http://host.docker.internal:8083/api/v1/products")
                )
                .route(p -> p
                        .path("/api/v1/products").and().method("POST")
                        .uri("http://host.docker.internal:8083/api/v1/products")
                )
                .route(p -> p
                        .path("/api/v1/products/category").and().method("GET")
                        .uri("http://host.docker.internal:8083/api/v1/products/category")
                )
                //***Supply
                .route(p -> p
                        .path("/api/v1/supply/add").and().method("POST")
                        .uri("http://host.docker.internal:8083/api/v1/supply/add")
                )
                .route(p -> p
                        .path("/api/v1/supply/update").and().method("POST")
                        .uri("http://host.docker.internal:8083/api/v1/supply/update")
                )
                .route(p -> p
                        .path("/api/v1/supply/**").and().method("GET")
                        .filters(f->f.rewritePath("api/v1/supply/(?<shopid>.*)","api/v1/supply/${shopid}"))
                        .uri("http://host.docker.internal:8083")
                )
                .route(p -> p
                        .path("/api/v1/supply/remove").and().method("DELETE")
                        .uri("http://host.docker.internal:8083/api/v1/supply/remove")
                )
                .route(p -> p
                        .path("/api/v1/supply/findSupplyById").and().method("POST")
                        .uri("http://host.docker.internal:8083/api/v1/supply/findSupplyById")
                )
                .route(p -> p
                        .path("/api/v1/supply/findShopByCategory").and().method("POST")
                        .filters(f -> f.addRequestHeader("content-type", "application/json"))
                        .uri("http://host.docker.internal:8083/api/v1/supply/findShopByCategory")
                )
                //***Order
                .route(p -> p
                        .path("/api/v1/order").and().method("POST")
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/order/updateStatus").and().method("POST")
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/order/findDone").and().method("POST")
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/order/findReceived").and().method("POST")
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/ElementCart/findByOrderId").and().method("POST")
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/order/**").and().method("GET")
                        .filters(f->f.rewritePath("/api/v1/order/(?<userId>.*)","api/v1/order/${userId}"))
                        .uri("http://host.docker.internal:8086")
                )
                .route(p -> p
                        .path("/api/v1/order/shop/**").and().method("DELETE")
                        .filters(f->f.rewritePath("/api/v1/order/(?<shopId>.*)","api/v1/order/${shopId}"))
                        .uri("http://host.docker.internal:8086")
                )
                .build();
    }

    public static void main(String[] args) {
        log.debug("[API Gateway Service] - On start");
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

}
